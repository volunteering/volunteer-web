# volunteer-web

Basic web administration for volunteering software

### purpose

This site controls a website of events and their users

Every user is free to sign-up on the system

Each user belongs to, or is grouped by a role, Thus some privileges are given to each user.

Each user start with a default role
